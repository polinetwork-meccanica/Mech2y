%% some simple commands

clear; close all; clc

1+2

1-2

1*2

1/2

2^2

(20 - (6 + 7) / 12) * 13 - 10 / (3 * 4)

(sin(30/180*pi))^2+(cos(30/180*pi))^2
(sin(deg2rad(30)))^2+(cos(deg2rad(30)))^2

sqrt(15)
15^(1/2)

exp(i*pi)+1
exp(j*pi)+1

%% Variables, vectors, matrices

clear; close all; clc

a=1;
b=2;
a+b

a=[1;4;2;6]
a=[1,4,2,6]'

b=[1,4,2,6]
b=[1 4 2 6]
b=a'

c=b*a

a=(1:4)'

b=0:0.1:0.3

a=linspace(0,2*pi,100)

%% matrices

clear; close all; clc

a=[1     2     3     4
    5     6     7     8
    9    10    11    12
    13    14    15    16]
a=[1:4
    5:8
    9:12
    13:16]

b=eye(4)

c=ones(4)

d=zeros(4,3)

c=repmat(1,4,4)

d=repmat(0,4,3)

e=a+b

f=a-b

g=3*a*c

a(1,2)

n=randn(12,8)
n=randn([12,8])

n1=normrnd(10,4,12,8)

A=[1 1 1
    2 3 -1
    -1 -3 2];
b=[2;4;4];
inv(A)*b
A\b

C=A(:,1:2);
C\b

%% Special matrix operators

clear; close all; clc

A=normrnd(0,1,3,3);
B=normrnd(0,1,3,3);

A.*B
A./B
A.^B

A.^2

sum(A)
mean(A)
std(A)

sum(A,2)
mean(A,2)
std(A,0,2)

A>0
B=A;
B(B<0)=0

%% Flow control operators

clear; close all; clc

% Create a 100 elements vector a from a distribution N(0,1).
% Verify if a contains either more positive or negative values,
% or the same number of positive and negative values.
% (hint: use if� elseif� else, disp)
a=normrnd(0,1,100,1);
if sum(a>0) > sum(a<0)
    disp('More positive values than negative where found');
elseif sum(a>0) < sum(a<0)
    disp('More negative values than positive where found');
else
    disp('The same number of positive and negative values was found')
end

% Generate a series of number from a statistical distribution N(3,1), and
% continue adding these points to a column vector a until a negative value
% appears. (Suggestion: use while... end).

attnumber=1;
a=zeros(1000,1);
index=1;
while attnumber>=0
    attnumber=normrnd(3,1);
    a(index,1)=attnumber;
    index=index+1;
end
a(index-1:end)=[];
    
% Generate one hundred numbers from a statistical distribution N(0,1) and
% store them in column vector a. Then, copy positive values into vector ap,
% negative values into vector an. (Suggestion: use for� end).

a=normrnd(0,1,100,1);
ap=zeros(size(a));
an=zeros(size(a));
indexp=1;
indexn=1;
for h=a'
    if h>0
        ap(indexp)=h;
        indexp=indexp+1;
    elseif h<0
        an(indexn)=h;
        indexn=indexn+1;
    end
end
ap(indexp:end)=[];
an(indexn:end)=[];

%% functions

clear; close all; clc

% Write a function that, given a vector a, copies positive values into
% vector ap, negative values into vector an.

a=normrnd(0,1,100,1);
[ap,an]=split_pn(a);

% Write a function that, given a point in cartesian coordinate x, y, z,
% returns point coordinate in cylindrical form z, t, r. (suggestion: use
% function atan).

x=normrnd(0,1,10,1);
y=normrnd(0,1,10,1);
z=normrnd(0,1,10,1);
[z,t,r]=fromxyztoztr(x,y,z);
[t1,r1,z1]=cart2pol(x,y,z); % this is the MATLAB built in function for the cylindrical conversion...

% Write a function which sorts a vector.

x=normrnd(0,1,100,1);
xs=ordina(x);

%% graphical representation

clear; close all; clc

x=linspace(-1,1,100);

% y=x

y=x;
plot(x,y);

% y=x^2

figure
y2=x.^2;
plot(x,y2);

% y=step(x)

figure
y3=zeros(size(x));
y3(x>=0)=1;
plot(x,y3);
axis([-1 1 -2 2])

% Plot these functions on a single graph. (hint : use hold)

figure; hold on
plot(x,y);
plot(x,y2);
plot(x,y3);
axis([-1 1 -2 2])

% Add label to the axes and a suitable legend. (hint : use xlabel, ylabel,
% legend)

figure; hold on
plot(x,y,'.g--');
plot(x,y2,'*y-.');
plot(x,y3,'or-');
axis([-1 1 -2 2])
legenda={'y=x'
    'y=x^2'
    'y=step(x)'};
legend(legenda,'Location','se')

%% 3D graphical representation

clear; close all; clc

% Plot the following mathematical function (x in [-1,1], y in [-1,1])
% (hint: use surf, meshgrid)

figure
x=linspace(-1,1,100);
y=x;
[x,y]=meshgrid(x,y);
z=cos(4*pi*x-7*pi*y+2);
plot3(x,y,z,'.')
figure
surf(x,y,z,'facecolor','interp','edgealpha',0.1);

% Plot the same function in wire frame. (hint : use mesh)

figure
mesh(x,y,z,x);

% Plot the following function in a circular domain centered around zero,
% with a radius one. (hint: use trisurf, delaunay, pol2cart)

figure
r=linspace(0.01,1,100);
t=linspace(0,2*pi*(0.99),100);
[r,t]=meshgrid(r,t);
r=r(:);t=t(:);
[x,y]=pol2cart(t,r);
z=x.^2+y.^2-0.5;
tria=delaunay(x,y);
trisurf(tria,x,y,z,'facecolor','interp','edgealpha',0.1);

% Plot a cube. (hint: use patch).

figure
z=[0;0;0;0;1;1;1;1];
x=[0;0;1;1;0;0;1;1];
y=[0;1;0;1;0;1;0;1];
vertici=[x,y,z];
facce=[1 2 4 3
    1 2 6 5
    1 3 7 5
    8 7 5 6
    8 4 3 7
    8 6 2 4];
patch('faces',facce,'vertices',vertici,'facecolor','g');
axis equal;

%% 3D graphical representation

clear; close all; clc

% Plot the following vector field (x in [-1,1], y in [-1,1]) (hint: use quiver)

x=linspace(-1,1,15);
y=x;
[x,y]=meshgrid(x,y);
a=sin(2*pi*x)+cos(2*pi*y);
b=sin(2*pi*y)+cos(2*pi*x);
quiver(x,y,a,b);

% Plot the following vector field (x in[-1,1], y in [-1,1], z in [-1,1])
% (hint: use quiver3)

figure
x=linspace(-1,1,10);
y=x;z=x;
[x,y,z]=meshgrid(x,y,z);
a=sin(2*pi*x)+cos(2*pi*y);
b=sin(2*pi*y)+cos(2*pi*x);
c=z.^2;
quiver3(x,y,z,a,b,c);
